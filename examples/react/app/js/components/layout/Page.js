import React from "react";
import Layout from "./Layout";

const Page = Component => {
	class Page extends React.Component {
		render() {
			return (
				<Layout>
					<Component />
				</Layout>
			);
		}
	}
	return Page;
};

export default Page;
