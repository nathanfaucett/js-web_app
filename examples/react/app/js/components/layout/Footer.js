import React from "react";
import { FormattedMessage } from "react-intl";
import app from "../../app";

class Footer extends React.Component {
    createOnClick(locale) {
        return () => {
            app.plugin(app.Locales).setLocale(locale);
        };
    }
    render() {
        return (
            <div>
                <p>
                    <FormattedMessage id="app.name" />
                </p>
                <button onClick={this.createOnClick("en")}>en</button>
                <span>|</span>
                <button onClick={this.createOnClick("de")}>de</button>
            </div>
        );
    }
}

export default Footer;
