import React from "react";
import LayoutNoHeader from "./LayoutNoHeader";

const PageNoHeader = Component => {
	class PageNoHeader extends React.Component {
		render() {
			return (
				<LayoutNoHeader>
					<Component />
				</LayoutNoHeader>
			);
		}
	}
	return PageNoHeader;
};

export default PageNoHeader;
