import React from "react";
import Footer from "./Footer";

class LayoutNoHeader extends React.Component {
    render() {
        return (
            <div>
                {this.props.children}
                <Footer />
            </div>
        );
    }
}

export default LayoutNoHeader;
