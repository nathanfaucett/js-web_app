import React from "react";
import { IntlProvider } from "react-intl";
import connect from "@nathanfaucett/state-immutable-react";
import app from "../app";

const routerPlugin = app.plugin(app.Router),
    routerStore = routerPlugin.store(),
    localeStore = app.plugin(app.Locales).store();

class Root extends React.Component {
    render() {
        const { router, locales } = this.props,
            { state } = router,
            Component = routerPlugin.data(state),
            locale = locales.locale,
            messages = locales.locales[locale];

        return (
            <div>
                {messages ? (
                    <IntlProvider locale={locale} messages={messages}>
                        {Component ? (
                            <Component />
                        ) : (
                            <p>
                                No Component for State {state}, this should not
                                happen, check you routes to see if you used a
                                React Component
                            </p>
                        )}
                    </IntlProvider>
                ) : (
                    <p>Loading Locales...</p>
                )}
            </div>
        );
    }
}

export default connect([routerStore, localeStore])(Root);
