import React from "react";
import { injectIntl } from "react-intl";
import connect from "@nathanfaucett/state-immutable-react";
import user from "../stores/user";
import app from "../app";

const signInForm = app.createStore("signInForm", {
    values: {
        email: "",
        password: ""
    },
    errors: {
        email: [],
        password: []
    }
});

class SignIn extends React.Component {
    constructor(props) {
        super(props);

        this.onSubmit = e => {
            e.preventDefault();

            if (this.form.isValid()) {
                const { email, password } = this.props.signInForm.values;

                user.signIn(email, password, () => {
                    app.page.go("/");
                    this.form.reset();
                });
            }
        };

        const { intl } = this.props,
            { email, password } = this.props.signInForm.values;

        this.form = app.createForm(
            {
                email: ({ onChange }) => ({
                    props: {
                        type: "text",
                        placeholder: intl.formatMessage({
                            id: "sign_in.email"
                        }),
                        value: email,
                        onChange: e => onChange(e.target.value)
                    }
                }),
                password: ({ onChange }) => ({
                    props: {
                        type: "password",
                        placeholder: intl.formatMessage({
                            id: "sign_in.password"
                        }),
                        value: password,
                        onChange: e => onChange(e.target.value)
                    }
                })
            },
            {
                get: () => ({ ...this.props.signInForm.values }),
                set: (errors, values) =>
                    signInForm.setState({ errors: errors, values: values }),
                changeset: changeset =>
                    changeset
                        .validateLength("password", { gte: 6 })
                        .validateFormat("email", /@/)
            }
        );
    }

    render() {
        const { intl } = this.props,
            { errors, values } = this.props.signInForm,
            fields = this.form.render(errors, values);

        return (
            <form onSubmit={this.onSubmit}>
                <input {...fields.email.props} />
                <Errors errors={fields.email.errors} />
                <br />
                <input {...fields.password.props} />
                <Errors errors={fields.password.errors} />
                <br />
                <input
                    type="submit"
                    value={intl.formatMessage({
                        id: "sign_in.sign_in"
                    })}
                    onClick={this.onSubmit}
                />
            </form>
        );
    }
}

const Errors = props => (
    <div>
        {props.errors.map((error, index) => (
            <p key={index}>
                {error.message} {error.keys.join(" ")}
            </p>
        ))}
    </div>
);

export default connect([signInForm])(injectIntl(SignIn));
