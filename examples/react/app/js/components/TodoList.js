import React from "react";
import connect from "@nathanfaucett/state-immutable-react";
import todos from "../stores/todos";
import app from "../app";
import Todo from "./Todo";

const todoListForm = app.createStore("todoListForm", {
    values: { text: "" },
    errors: { text: [] }
});

class TodoList extends React.Component {
    constructor(props) {
        super(props);

        this.form = app.createForm(
            {
                text: ({ onChange }) => ({
                    props: {
                        type: "text",
                        value: props.todoListForm.text,
                        onChange: e => onChange(e.target.value)
                    }
                })
            },
            {
                get: () => ({ ...this.props.todoListForm.values }),
                set: (errors, values) =>
                    todoListForm.setState({ errors, values })
            }
        );

        this.onDeleteTodo = id => {
            todos.remove(id);
        };
        this.onSubmit = e => {
            e.preventDefault();
            todos.create(this.props.todoListForm.values.text);
            this.form.reset();
        };
    }

    render() {
        var { values, errors } = this.props.todoListForm,
            fields = this.form.render(errors, values);

        return (
            <div className="TodoList">
                <form onSubmit={this.onSubmit}>
                    <input {...fields.text.props} />
                </form>
                <div>
                    {this.props.todos.list.map(todo => {
                        return (
                            <Todo
                                key={todo.id}
                                todo={todo}
                                onClick={this.onDeleteTodo}
                            />
                        );
                    })}
                </div>
            </div>
        );
    }
}

export default connect([todos, todoListForm])(TodoList);
