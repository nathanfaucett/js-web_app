import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { addLocaleData } from "react-intl";
import en from "react-intl/locale-data/en";
import de from "react-intl/locale-data/de";
import Root from "./components/Root";
import app from "./app";
import "./routes";

addLocaleData([...en, ...de]);

app.on("load", () => {
	render(<Root />, document.getElementById("root"));
});

if (module.hot) {
	module.hot.accept(() => {
		if (app.initted()) {
			const rootNode = document.getElementById("root");

			app.plugin(app.Locales).reloadLocale();

			unmountComponentAtNode(rootNode);
			render(<Root />, rootNode);
		}
	});
}
