import { fromJS } from "immutable";
import uuid from "@nathanfaucett/uuid";
import app from "../app";

const todos = app.createStore("todos", { list: [] });

todos.create = text => {
    todos.updateState(state =>
        state.update("list", list =>
            list.push(fromJS({ id: uuid.v4(), text: text }))
        )
    );
};

todos.remove = id => {
    todos.updateState(state => {
        const list = state.get("list"),
            index = list.findIndex(todo => todo.get("id") === id);

        if (index === -1) {
            return state;
        } else {
            return state.set("list", list.remove(index));
        }
    });
};

export default todos;
