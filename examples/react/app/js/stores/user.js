import app from "../app";

const USER_TOKEN = "X-WebAppExample-User-Token";

const user = app.createStore("user", {
    id: null,
    token: null,
    username: ""
});

user.isSignedIn = () => {
    return user.state().get("token") !== null;
};

user.autoSignIn = callback => {
    if (!user.isSignedIn()) {
        const token = app.cookies.get(USER_TOKEN);

        if (token) {
            user.updateState(state =>
                state
                    .set("id", 1)
                    .set("token", "1234567890")
                    .set("username", "username")
            );

            callback();
        } else {
            callback(new Error("No Token Available"));
        }
    } else {
        callback();
    }
};

user.signIn = (username, password, callback) => {
    app.cookies.set(USER_TOKEN, "1234567890");

    user.updateState(state =>
        state
            .set("id", 1)
            .set("token", "1234567890")
            .set("username", "username")
    );

    if (callback) {
        callback();
    }
};

user.signOut = callback => {
    app.cookies.remove(USER_TOKEN);

    user.updateState(state =>
        state
            .set("id", null)
            .set("token", null)
            .set("username", null)
    );

    if (callback) {
        callback();
    }
};

export default user;
