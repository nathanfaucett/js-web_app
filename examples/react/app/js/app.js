import { App } from "@nathanfaucett/web_app";
import config from "./config.json";

class WebAppExampleApp extends App {
    constructor(config) {
        super(config, {
            restoreAppStateOnLoad: false,
            appStateKey: "WebAppExample-State",
            Locales: {
                header: "X-WebAppExample-Locale",
                locales: {
                    en: import("../locales/en.json"),
                    de: import("../locales/de.json")
                }
            }
        });

        this.request.defaults.headers.Accept = "application/json";
        this.request.defaults.headers["Content-Type"] = "application/json";
        this.request.defaults.withCredentials = true;
    }
}

export default new WebAppExampleApp(config);
