import Page from "./components/layout/Page";
import PageNoHeader from "./components/layout/PageNoHeader";
import SignIn from "./components/SignIn";
import TodoList from "./components/TodoList";
import NotFound from "./components/NotFound";
import app from "./app";
import autoSignIn from "./middleware/autoSignIn";

const router = app.plugin(app.Router);

router.route("sign_in", "/sign_in", PageNoHeader(SignIn));

// all routes after this the user must be signed in
// ===============================================================
router.use(autoSignIn);

// Create a scope
// ===============================================================
const rootScope = router.scope("/");
rootScope.route("index", "/", Page(TodoList));

// Handle no match case
// ===============================================================
router.use(router.notFound(Page(NotFound)));
