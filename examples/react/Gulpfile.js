const gulp = require("gulp"),
    ParcelBundler = require("parcel-bundler"),
    express = require("express"),
    del = require("del"),
    gulpUtil = require("gulp-util"),
    configBundler = require("@nathanfaucett/config-bundler"),
    localesBundler = require("@nathanfaucett/locales-bundler");

const IS_PROD = process.env.NODE_ENV === "production",
    IS_TEST = process.env.NODE_ENV === "test",
    IS_DEV = !IS_PROD,
    SERVER_PORT = IS_TEST ? 8001 : process.env.PORT || 8000,
    API_SERVER_PORT = process.env.API_PORT || 4000;

const cleanCache = () => del(["./.cache"]);

gulp.task("clean-cache", cleanCache);

const clean = () => del(["./app/js/config.json", "./build"]);

gulp.task("clean", clean);

const config = () => {
    return gulp
        .src(["./config/*.js"])
        .pipe(
            configBundler({
                options: {
                    PORT: SERVER_PORT,
                    API_PORT: API_SERVER_PORT
                }
            })
        )
        .pipe(gulp.dest("./app/js"));
};

gulp.task("config", config);

const locales = () => {
    return gulp
        .src("./config/locales/**/*.json")
        .pipe(
            localesBundler({
                flatten: true,
                minify: IS_PROD
            })
        )
        .pipe(gulp.dest("./app/locales"));
};

gulp.task("locales", locales);

const parcel = watch => () => {
    const bundler = new ParcelBundler("./app/index.html", {
        outDir: "./build",
        outFile: "index.html",
        publicUrl: "./",

        watch: watch === false ? false : IS_DEV,
        cache: IS_DEV,
        cacheDir: ".cache",

        minify: IS_PROD,
        target: "browser",
        https: IS_PROD,
        contentHash: IS_PROD,

        logLevel: 3,
        hmrPort: 0,
        sourceMaps: true,
        hmrHostname: "",
        detailedReport: false
    });

    return bundler.bundle();
};

gulp.task("parcel", parcel());

const serve = () => {
    const app = express();
    app.use(express.static("build"));
    app.listen(SERVER_PORT);
    gulpUtil.log("Listening on port " + SERVER_PORT);
};

gulp.task("serve", serve);

const watch = () => {
    gulp.watch(["./config/*.js"], config);
    gulp.watch("./config/locales/**/*.json", locales);
};

gulp.task("watch", watch);

const build = IS_PROD
    ? gulp.series(
          cleanCache,
          clean,
          gulp.parallel(config, locales),
          parcel(false)
      )
    : gulp.series(clean, gulp.parallel(config, locales), parcel(false));

gulp.task("build", build);

const run = IS_PROD
    ? gulp.series(cleanCache, clean, gulp.parallel(config, locales), parcel())
    : gulp.series(
          clean,
          gulp.parallel(config, locales),
          parcel(),
          gulp.parallel(serve, watch)
      );

gulp.task("run", run);

gulp.task("default", run);
