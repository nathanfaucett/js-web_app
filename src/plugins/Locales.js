var browserLangauge = require("@jwaterfaucett/browser_langauge"),
    Plugin = require("../Plugin");

var LocalesPrototype;

module.exports = Locales;

function Locales(app, options) {
    var _this = this,
        defaultLocale,
        store;

    Plugin.call(this, app, options);

    this.header = this.options.header || "X-WebApp-Locale";
    this.locales = this.options.locales || {};

    defaultLocale =
        this.app.cookies.get(this.header) ||
        browserLangauge.getPreferredLanguage("en");

    store = this._store = this.app.createStore(Locales.storeName(), {
        locale: defaultLocale,
        locales: {}
    });

    this._loading = {};
    this._loaded = {};

    store.on("unsafeSetState", function onUnsafeSetState(state) {
        _this.loadLocale(state.get("locale"));
    });
    store.on("update", function onUpdate() {
        _this.loadLocale(store.state().get("locale"));
    });

    store.setLocale = function(locale) {
        return _this.setLocale(locale);
    };
    store.setLocaleDefault = function() {
        return _this.setLocaleDefault();
    };
    store.forceLoadLocale = function(locale) {
        return _this.forceLoadLocale(locale);
    };
    store.loadLocale = function(locale) {
        return _this.loadLocale(locale);
    };
    store.reloadLocale = function() {
        return _this.reloadLocale();
    };

    this.loadLocale(defaultLocale);
}
Plugin.extend(Locales);
LocalesPrototype = Locales.prototype;

Locales.pluginName = function() {
    return "Locales";
};

Locales.storeName = function() {
    return "locales";
};

LocalesPrototype.store = function() {
    return this._store;
};

LocalesPrototype.setLocale = function(locale) {
    this.app.cookies.set(this.header, locale);
    this._store.updateState(state => state.set("locale", locale));
};

LocalesPrototype.setLocaleDefault = function() {
    this.setLocale(browserLangauge.getPreferredLanguage("en"));
};

LocalesPrototype.forceLoadLocale = function(locale) {
    var _this = this;

    this._loading[locale] = true;

    this.locales[locale].then(function onThen(rawLocales) {
        _this._loading[locale] = false;
        _this._loaded[locale] = true;

        _this._store.updateState(function onUpdate(state) {
            return state.update("locales", function update(locales) {
                return locales.set(locale, rawLocales);
            });
        });
    });
};

LocalesPrototype.loadLocale = function(locale) {
    if (!this._loading[locale] && !this._loaded[locale]) {
        this.forceLoadLocale(locale);
    }
};

LocalesPrototype.reloadLocale = function() {
    this.forceLoadLocale(this._store.state().get("locale"));
};
