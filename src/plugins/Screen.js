var Plugin = require("../Plugin");

var ScreenPrototype;

module.exports = Screen;

function Screen(app, options) {
    var store;

    Plugin.call(this, app, options);

    store = this._store = this.app.createStore(Screen.storeName(), {
        width: window.innerWidth,
        height: window.innerHeight
    });

    this.app.addEventListener(
        window,
        "resize orientationchange",
        function onResize() {
            store.setState({
                width: window.innerWidth,
                height: window.innerHeight
            });
        }
    );
}
Plugin.extend(Screen);
ScreenPrototype = Screen.prototype;

Screen.pluginName = function() {
    return "Screen";
};
Screen.storeName = function() {
    return "screen";
};

ScreenPrototype.store = function() {
    return this._store;
};
